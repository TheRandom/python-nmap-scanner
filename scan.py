#!/usr/bin/python3
# -*- coding: utf-8 -*-

import nmap
import os
import time

logo = """
Python Scanner by the Random
"""
#For more info use the dictionnary after the scan:
#	scanner[ip_addr]['tcp'][list(result)[i]]

scanner = nmap.PortScanner()

print(logo)


ip_addr = input("ip address : ")
port = input("port-range: ")

resp = input("""\nList of Actions
				1) SYN ACK Scan (Stealth)
				2) UDP Scan
				3) Comprehensive Scan\n>""")
print("[*] Running Scan ...")
time.sleep(1.5)
os.system("clear")

if(resp == '1'):
	print("[*] Nmap version: ", scanner.nmap_version())
	#Stealth nmap scan argument
	scanner.scan(ip_addr, port, "-v -sS")
	print(scanner.scaninfo())
	print("[-] IP Status : ", scanner[ip_addr].state())
	print("[*] Protocols used: ", scanner[ip_addr].all_protocols())
	#list of open ports by diplaying dict_keys
	print("[*] Open Ports: ", scanner[ip_addr]['tcp'].keys())
	result = scanner[ip_addr]['tcp'].keys()
	# For each port display the number and the service that is associated to it
	for i in range(0,len(list(result))):
		print("Port ", list(result)[i], "is open protocol used: ", scanner[ip_addr]['tcp'][list(result)[i]]['name'])
	print("[*] Done")


elif(resp == '2'):
	print(" Nmap version: ", scanner.nmap_version())
	scanner.scan(ip_addr, port, "-v -sU")
	print(scanner.scaninfo())
	print("[-] IP Status : ", scanner[ip_addr].state())
	print("[*] Protocols used: ", scanner[ip_addr].all_protocols())
	print("[*] Open Ports: ", scanner[ip_addr]['udp'].keys())
	result = scanner[ip_addr]['udp'].keys()
	for i in range(0,len(list(result))):
		print("Port ", list(result)[i], "is open protocol used: ", scanner[ip_addr]['udp'][list(result)[i]]['name'])
	print("[*] Done")

elif(resp == '3'):
	print(" Nmap version: ", scanner.nmap_version())
	scanner.scan(ip_addr, port, "-v -sS -sV -sC -A -O")
	print(scanner.scaninfo())
	print("[-] IP Status : ", scanner[ip_addr].state())
	print("[*] Protocols used: ", scanner[ip_addr].all_protocols())
	print("[*] Open Ports: ", scanner[ip_addr]['tcp'].keys())
	result = scanner[ip_addr]['tcp'].keys()
	for i in range(0,len(list(result))):
		print("Port ", list(result)[i], "is open protocol used: ", scanner[ip_addr]['tcp'][list(result)[i]]['name'])
	print("[*] Done")

elif(resp >= '4'):
	print("[-] Error unvalid option")
	print("[!] Exiting ...")
	time.sleep(3)

quit = input("Type on any key to exit\n")
