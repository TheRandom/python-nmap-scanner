# Python-nmap-scanner

This is a simple tool using nmap module to easily run scans and receiving informations
This program has been tested on Linux and Windows

# Pre-requisite
You need python 3 and python-nmap module installed to do so :
- apt install python3
- pip3 install python-nmap

# Next Step
This program will be updated very soon to add other features as writing result in file